## Table of Contents
1. [Until Week 14](#until-week-14)
2. [Week 15](#week-15)
3. [Week 16](#week-16)
4. [Week 17](#week-17)
5. [Week 18](#week-18)
6. [Week 19](#week-19)
7. [Week 22](#week-22)
8. [MAI Cup Week 3](#week-3)

## Until Week 14
- First Display Code
- First Teamselect Code
- Working Driving Codes (with perfect accuracy)
- Working with Raspberry Pi Pico

## Week 15
- First Enemy-Detection-Code

## Week 16
- Rebuilt Bot Hardware
- Replanned 3d model
    - planned top plate
    - planned middle plate
- First tactics
- first wiring diagram ([Link](https://1drv.ms/u/s!AjWxvMBValH5gpd5g7pLX6WjrYQN9w?e=zXcB5L))

## Week 17
- Started on Serial Communication Code
- Began finishing the hardware
- Started wiring up the bot
- First ideas of pico-core-planning
- Sorted project for dual-picos
- Rethought hardware and wiring
- Began real wiring diagram ([Link](https://1drv.ms/b/s!AjWxvMBValH5grtaJsN9BEBS15s7EQ?e=ZS4ieH))

## Week 18
- Ideas for Power Board, Servo boards and a pico pcb board
- First basket ideas
- Bot gripper system done, new tires, rebuilt bot
- we will make the code uniform
- Serial Communication done
## Week 19
- Bot Hardware done
- Gripper Code
- US Sensor Ideas: PCB Boards and 3D-printed holders for enemy-detection
- Drive code
- Started line sensor testing

## Week 22
- Tested our first code
- Done sensor pico communication
- Cherry basket done
- Starting to build playing area

## Until Week 26
- Got our final version of our main tactic
- Written drive codes for our bot
- Tested a lot
- Hardware done, Software started and tested

## Until contest
### Holydays

- Arena Codes (Taktik 1-3)
- Automatic homing auf allen Startzonen
- Enums für Condition beim Fahren
- vscode include path, ds_store delete script
- von .h → .h / .cpp files (correct include system, fix all include problems)
- globals.h / logic.h
    - all needed include files in one header file
    - add main.h for easier compiling
- Umstieg auf PlatformIO
- teamBlue logic (before turning read team and reverse direction)
- team gets selected at startup using teamselect-switch
- PIO servo fix, [multicore fix,](https://gitlab.com/MAI-Robotics/eurobot/eurobot-2023/small-bot/-/compare/f02b16d2af4ea67a78bbc1009902854213dd9a6f...db01b7bade8fa2a493e52b4aa4f3be5683572ca3) header files included fix, all compiler errors fixed
[https://gitlab.com/MAI-Robotics/eurobot/eurobot-2023/small-bot/-/compare/1272dd4b7f73ef280461cfea76992595900e12cf...89233a74247c8319c4aa312055617aaae09fe102](https://gitlab.com/MAI-Robotics/eurobot/eurobot-2023/small-bot/-/compare/1272dd4b7f73ef280461cfea76992595900e12cf...89233a74247c8319c4aa312055617aaae09fe102)
- New sensors.h / sensors.cpp for line sensor system ([Link](https://gitlab.com/MAI-Robotics/eurobot/eurobot-2023/small-bot/-/compare/5c2e896ebee90b4dcaee9fbd47416749762357a0...4bd39aa965524125c416d81125cf90386ba4d324))
- new routines for homing and settings pins
- pullcord neu gelötet kurz
- taktik struktur
- add function for sending an amount of values to logic pico per seconds (adjustable)
- code for following line (wont be used, not accelerated or anything else)
- coordinate system (driveTo(X, Y) → calculates angle and distance by itself)
    - distance tracking while driving
- Debug system, enable one bool and get proper outputs
- Add turnTo, setPos and resetPos
- CoA bool to enable globally
- 3 tactics to x-y system
- Basket and holder routines
- CoA: grouped to fix inaccuracies

### While testing

- CoA enabling default toggle
- automatic driving home by tracking time
- end Routine to drive into final zone
- clean up code: drive automatically further/back in basket and holder routines
- setting pos now thinks of bots lenght
- CoA now applies for direction driven, when turning all US will be used
- add a ton of settings in `logic.h`
- tactics(1) and tactics(4) are tested completely
- fixed turning through `directRotation(&turn);`
    - bot uses fastest way all the time
- fix distance tracking fw, bw and when turning
    - `x_pos = (dir ? x_pos + distancePerStep * cos(direction*PI/180) : x_pos - distancePerStep *cos(direction*PI/180));`
    - `y_pos = (dir ? y_pos + distancePerStep * sin(direction*PI/180) : y_pos - distancePerStep *sin(direction*PI/180));`
- driving further to wall, basket or cherry holder to reduce inclined driving

## Week 3
-> MAI Cup contest begin

- Initialized Repo
