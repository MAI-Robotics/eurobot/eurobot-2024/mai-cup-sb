## /test/0 whole bot
This folder is meant for test files concerning the whole bot, including Sensor-Pico and Logic-Pico.

## /test/1 parts
This folder is meant for test files concerning parts used or not used in the bot. In this folder there won't be any logic codes for the Eurobot.