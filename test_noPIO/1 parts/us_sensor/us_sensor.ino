int readUsSensor(int trigPin, int echoPin){
    // Clears the TRIG_PIN
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    // Sets the TRIG_PIN on HIGH state for 10 micro seconds
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    // Reads the ECHO_PIN, returns the sound wave travel time in microseconds
    int duration = pulseIn(echoPin, HIGH);
    // Calculating the distance
    int distance = duration * 0.034 / 2;

    return distance;
}

void setup() {
    Serial.begin(115200); // Starts the serial communication

    //pinModes
    pinMode(17, OUTPUT);
    pinMode(18, INPUT);
    pinMode(19, INPUT);
    pinMode(20, INPUT);
    pinMode(21, INPUT);

}
void loop() {

    Serial.print("back left: ");
    Serial.println(readUsSensor(17, 18));
    delay(20); 
    
    Serial.print("back right: ");
    Serial.println(readUsSensor(17, 19));
    delay(20); 
    Serial.print("front left: ");
    Serial.println(readUsSensor(17, 20));
    delay(20); 
    Serial.print("front right: ");
    Serial.println(readUsSensor(17, 21));
    
    delay(500);


    Serial.println("############################################");

}