/*
  by Emanuel Berger
  03.11.2022
*/

#include <Servo.h>      //library
#define MG995_1_Pin 9   //define pin

Servo MG995_1;          //new servo
int pos;                //turning var
enum Direction {CW, CCW}; //CW: clockwise; CCW: counterclockwise

void startServo() {
  MG995_1.attach(MG995_1_Pin);
}

void stopServo() {
  MG995_1.detach();
}

void turnServo(Direction direction, int degrees, int speed = 15) {
  if(direction == CW) {
    for(pos = 0; pos < degrees; pos += 1) {  // von 0 bis 180 Grad, in Schritten von einem Grad
      MG995_1.write(pos);                   // sagt dem Servomotor, in welche Position sich drehen soll      
      delay(speed);                            // wartet 15 Millisekunden   
    }    
  }
  else if(direction == CCW) {
    for(pos = degrees; pos>=1; pos-=1) {     // und das gleiche zurück
      MG995_1.write(pos);
      delay(speed);
    }
  }
}

void setup() {
  startServo();
}

void loop() {
  startServo();
  turnServo(CW, 400);
  delay(1000);
  turnServo(CCW, 100);
  delay(1000);
  turnServo(CW, 400);
  stopServo();
  delay(5000);
}