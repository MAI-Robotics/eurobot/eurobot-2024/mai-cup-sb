#include <Servo.h> // Servo library by Arduino used https://www.arduino.cc/reference/en/libraries/servo/ (already integrated in Arduino IDE)
#define Servo_PWM 6 //Servo PWM Pin
Servo MG995_Servo;

enum Direction {CW, CCW}; //CW: clockwise; CCW: counterclockwise


/**
 * @description: tells the servo the direction
 * @param direction: CW(clockwise)/CCW(counterclockwise)
*/
void servoSpin(Direction direction) { 
  MG995_Servo.attach(Servo_PWM); //Start, use after detach 
  if(direction == CW) {
    Serial.println("Turning clockwise");
    MG995_Servo.write(0);
  } else if(direction == CCW) {
    Serial.println("Turning counterclockwise");
    MG995_Servo.write(180);
  }
}

void servoStop() {
  MG995_Servo.detach(); //Stop
}


//------


void setup() {
  Serial.begin(9600); //Start Serial Monitor with 9600 Baud rate
}

void loop() {
  servoSpin(CW);
  delay(3000);

  servoStop();
  delay(2000);

  servoSpin(CCW);
  delay(3000);

  servoStop();
  delay(2000);
}
