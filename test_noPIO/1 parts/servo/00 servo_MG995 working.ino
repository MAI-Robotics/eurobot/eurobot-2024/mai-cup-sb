#include <Servo.h>

Servo servoArm; //Erstellt Servo alst Servo
#define servoArmPin 10

void servoDrehen(int winkel) { //Methode zum Servo drehen
    servoArm.write(winkel); //Schreibt den Winkel auf den den Servo
}



void setup() {
    Serial.begin(9600);
    servoArm.attach(servoArmPin); //Attach Servo auf Pin
    servoArm.write(0); //Setz den Servo auf 0 Grad zurück
}

void loop() {
    delay(5000);
    servoDrehen(90);// Servo dreht sich auf 90 Grad (von 0 aus gesehen)
    delay(5000);
    servoDrehen(0);// Servo dreht sich wieder auf 0
}