#include <Arduino.h>
#include <NewPing.h>
#include "pinsSensors.h"

#define MAX_DISTANCE 300

NewPing usSensorFrontLeft(US_TRIG_LEFT, US_FRONT_LEFT_ECHO_PIN, MAX_DISTANCE);
NewPing usSensorFrontRight(US_TRIG_RIGHT, US_FRONT_RIGHT_ECHO_PIN, MAX_DISTANCE);
NewPing usSensorRight(US_TRIG_RIGHT, US_RIGHT_ECHO_PIN, MAX_DISTANCE);
NewPing usSensorBackRight(US_TRIG_RIGHT, US_BACK_RIGHT_ECHO_PIN, MAX_DISTANCE);
NewPing usSensorBackLeft(US_TRIG_LEFT, US_BACK_LEFT_ECHO_PIN, MAX_DISTANCE);
NewPing usSensorLeft(US_TRIG_LEFT, US_LEFT_ECHO_PIN, MAX_DISTANCE);

void setup()
{ 
    Serial.begin(115200); 

    //pinModes
    pinMode(US_TRIG_LEFT, OUTPUT);
    pinMode(US_FRONT_LEFT_ECHO_PIN, INPUT);

    pinMode(US_TRIG_RIGHT, OUTPUT);
    pinMode(US_FRONT_RIGHT_ECHO_PIN, INPUT);

    pinMode(US_RIGHT_ECHO_PIN, INPUT);

    pinMode(US_BACK_RIGHT_ECHO_PIN, INPUT);

    pinMode(US_BACK_LEFT_ECHO_PIN, INPUT);

    pinMode(US_LEFT_ECHO_PIN, INPUT);

    Serial.println("Setup for US sensor test complete");

}

void loop()
{
    Serial.print("Sensor front left:  ");
    Serial.println( usSensorFontLeft.ping_cm() ); //return current distance (cm) in serial

    delay(20);

    Serial.print("Sensor front right:  ");
    Serial.println( usSensorFrontRight.ping_cm() ); //return current distance (cm) in serial

    delay(20);

    Serial.print("Sensor right:  ");
    Serial.println( usSensorRight.ping_cm() ); //return current distance (cm) in serial

    delay(20);

    Serial.print("Sensor back right:  ");
    Serial.println( usSensorBackRight.ping_cm() ); //return current distance (cm) in serial

    delay(20);

    Serial.print("Sensor back left:  ");
    Serial.println( usSensorBackLeft.ping_cm() ); //return current distance (cm) in serial

    delay(20);

    Serial.print("Sensor left:  ");
    Serial.println( usSensorLeft.ping_cm() ); //return current distance (cm) in serial

    delay(20);

    delay(1000);

    Serial.println("########################################################");
}