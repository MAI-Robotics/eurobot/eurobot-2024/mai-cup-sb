#include "pins.h"

void setup(){
    pinMode(left_limit_switch, INPUT_PULLUP);
    pinMode(right_limit_switch, INPUT_PULLUP);
    pinMode(lower_rail_switch, INPUT_PULLUP);
    pinMode(pullcord, INPUT_PULLUP);
    pinMode(upper_rail_switch, INPUT_PULLUP);
    pinMode(team_select, INPUT_PULLUP);

    pinMode(25, OUTPUT);
}

void loop(){
    if(digitalRead(left_limit_switch)  == LOW || digitalRead(right_limit_switch)  == LOW || digitalRead(lower_rail_switch)  == LOW || digitalRead(upper_rail_switch)  == LOW || digitalRead(team_select)  == LOW){
        digitalWrite(25,HIGH);
        delay(50);
    } else{
        digitalWrite(25,LOW);
        delay(50);
    }
}