#include <QTRSensors.h>

/*
#define NUM_SENSORS 6
#define TIMEOUT 2500
#define EMITTER_PIN 2

QTRSensorsRC qtrrc((unsigned char[]) {A0, A1, A2, A3, A4, A5},
  NUM_SENSORS, TIMEOUT, EMITTER_PIN);


#define MOTOR1_STEP 8
#define MOTOR1_DIR 9
#define MOTOR2_STEP 10
#define MOTOR2_DIR 11
*/
void setup()
{
    Serial.begin(9600);
    //stepper motor setup
    pinMode(right_stepper_DIR, OUTPUT);
    pinMode(right_stepper_STEP, OUTPUT);
    pinMode(left_stepper_DIR, OUTPUT);
    pinMode(left_stepper_STEP, OUTPUT);

    digitalWrite(MOTOR1_DIR, HIGH);
    digitalWrite(MOTOR2_DIR, HIGH);

    startUpRoutine();

    delay(1000);
}

void loop()
{
  unsigned int sensors[NUM_SENSORS];
  int position = qtr.readLine(sensors);
  int error = position - 2500;

  Serial.print("Position: ");
  Serial.print(position);
  Serial.print("  Error: ");
  Serial.println(error);

  if (error < -1000)
  {
    digitalWrite(MOTOR1_DIR, HIGH);
    digitalWrite(MOTOR2_DIR, HIGH);

    digitalWrite(MOTOR1_STEP, HIGH);
    digitalWrite(MOTOR2_STEP, HIGH);
    delayMicroseconds(100);
    digitalWrite(MOTOR1_STEP, LOW);
    digitalWrite(MOTOR2_STEP, LOW);
    delayMicroseconds(100);
  }
  else if (error > 1000)
  {
    digitalWrite(MOTOR1_DIR, LOW);
    digitalWrite(MOTOR2_DIR, LOW);

    digitalWrite(MOTOR1_STEP, HIGH);
    digitalWrite(MOTOR2_STEP, HIGH);
    delayMicroseconds(100);
    digitalWrite(MOTOR1_STEP, LOW);
    digitalWrite(MOTOR2_STEP, LOW);
    delayMicroseconds(100);
  }
  else
  {
    digitalWrite(MOTOR1_STEP, LOW);
    digitalWrite(MOTOR2_STEP, LOW);
  }
}
