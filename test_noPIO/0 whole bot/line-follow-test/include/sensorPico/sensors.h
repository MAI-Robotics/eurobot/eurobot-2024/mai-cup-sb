// us sensors
int usSensorCount = 10;

float usFilteredValues[9];

//QTRX sensor
QTRSensors qtr;

const uint8_t irSensorCount = 6;
float irFilteredValues[irSensorCount+1];
uint16_t irRawValues[irSensorCount];
uint16_t irRawPosition;

int filter(int value, float* lastValue){
    float a = 0.3;

    Serial.print(value);
    Serial.print(",");

    float filteredValue = ((1-a)*(*lastValue)) + (a*value);
    *lastValue = filteredValue;

    Serial.println(filteredValue);

    return filteredValue;

}

int readUsSensor(int trigPin, int echoPin){
    // Clears the TRIG_PIN
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    // Sets the TRIG_PIN on HIGH state for 10 micro seconds
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    // Reads the ECHO_PIN, returns the sound wave travel time in microseconds
    int duration = pulseIn(echoPin, HIGH);
    // Calculating the distance
    int distance = duration * 0.034 / 2;

    return distance;
}

void readUS(){
    // read and filter us sensors
    usFilteredValues[0] = filter(readUsSensor(US_LEFT_TRIG, US_LEFT_ECHO), &usFilteredValues[0]);
    delay(10);
    usFilteredValues[1] = filter(readUsSensor(US_LEFT_TRIG, US_LEFT_FRONT_ECHO), &usFilteredValues[1]);
    delay(10);
    usFilteredValues[2] = filter(readUsSensor(US_LEFT_TRIG, US_LEFT_BACK_ECHO), &usFilteredValues[2]);
    delay(10);
    usFilteredValues[3] = filter(readUsSensor(US_RIGHT_TRIG, US_RIGHT_ECHO), &usFilteredValues[3]);
    delay(10);
    usFilteredValues[4] = filter(readUsSensor(US_RIGHT_TRIG, US_RIGHT_FRONT_ECHO), &usFilteredValues[4]);
    delay(10);
    usFilteredValues[5] = filter(readUsSensor(US_RIGHT_TRIG, US_RIGHT_BACK_ECHO), &usFilteredValues[5]);
    delay(10);
    usFilteredValues[6] = filter(readUsSensor(US_LOWER_TRIG, US_FRONT_LEFT_ECHO), &usFilteredValues[6]);
    delay(10);
    usFilteredValues[7] = filter(readUsSensor(US_LOWER_TRIG, US_FRONT_RIGHT_ECHO), &usFilteredValues[7]);
    delay(10);
    usFilteredValues[8] = filter(readUsSensor(US_LOWER_TRIG, US_BACK_LEFT_ECHO), &usFilteredValues[8]);
    delay(10);
    usFilteredValues[9] = filter(readUsSensor(US_LOWER_TRIG, US_BACK_RIGHT_ECHO), &usFilteredValues[9]);
}

void readIR(){
    
    // read raw ir values 
    irRawPosition = qtr.readLineBlack(irRawValues);

    // filter raw ir values
    for (int i = 0; i < irSensorCount; i++)
    {  
        //irFilteredValues[i] = filter(irRawValues[i], &irFilteredValues[i]);
        irFilteredValues[i] = irRawValues[i];
    }
    irFilteredValues[irSensorCount] = irRawPosition;
}

void sendData(){
    for(int i = 0; i < usSensorCount; i++){
        Serial1.print(usFilteredValues[i]);
        Serial1.print(";");
    }

    for (int j = 0; j < irSensorCount; j++){
        Serial1.print(irFilteredValues[j]);
        Serial1.print(";");
    }

    Serial1.println(String(qtr.readLineBlack(irRawValues)));
}

void printData(){
    
    Serial.println("US");

    for(int i = 0; i < usSensorCount; i++){
        Serial.print(i+1);
        Serial.print(": ");
        Serial.println(usFilteredValues[i]);

    }

    Serial.println("IR");

    for (int j = 0; j < irSensorCount; j++){
        Serial.print(j+1);
        Serial.print(": ");
        Serial.println(irFilteredValues[j]);
    }

    Serial.print("Position: ");

    Serial.println(String(qtr.readLineBlack(irRawValues)));

}

void calibrateQTRX(){
        for (uint16_t i = 0; i < 400; i++)
    {
        qtr.calibrate();
    }

    // print the calibration minimum values measured when emitters were on
    for (uint8_t i = 0; i < irSensorCount; i++)
    {
        Serial.print(qtr.calibrationOn.minimum[i]);
        Serial.print(' ');
    }
    Serial.println();

    // print the calibration maximum values measured when emitters were on
    for (uint8_t i = 0; i < irSensorCount; i++)
    {
        Serial.print(qtr.calibrationOn.maximum[i]);
        Serial.print(' ');
    }
    Serial.println();
    delay(1000);
}