#include "pins.h"
#include "servo_drive.h"
#include "stepper_drive.h"

void setup(){
    //limit switch setup
    pinMode(lower_rail_switch, INPUT_PULLUP);
    pinMode(upper_rail_switch, INPUT_PULLUP);
    pinMode(left_limit_switch, INPUT_PULLUP);
    pinMode(right_limit_switch, INPUT_PULLUP);

    //dc motor setup
    pinMode(right_PWM, OUTPUT);
    pinMode(left_PWM, OUTPUT);

    //stepper motor setup
    pinMode(right_stepper_DIR, OUTPUT);
    pinMode(right_stepper_STEP, OUTPUT);
    pinMode(left_stepper_DIR, OUTPUT);
    pinMode(left_stepper_STEP, OUTPUT);

    //servo setup
    LEFT_SERVO.attach(left_servo);
    RIGHT_SERVO.attach(right_servo);
    MAIN_SERVO.attach(center_servo);

    pinMode(25, OUTPUT);
    delay(100);
    //driveFast(10000, true);

    moveUp();
    delay(1000);
    lowerArm();
    delay(1000);
    openGripperBasket();
    moveDown();
    closeGripper();
    delay(1000);
    moveUp();
    delay(1000);
    driveFastWithoutSwitch(2000, false);
    liftArm();
    delay(1000);
    turnFast(720, true);
    driveFast(2000, true);
    delay(1000);
    openGripperBasket();
    

}
void loop(){
    
}

