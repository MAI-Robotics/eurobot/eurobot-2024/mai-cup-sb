//limit switch pins
#define upper_rail_switch 11
#define lower_rail_switch 13
#define right_limit_switch 14
#define left_limit_switch 15

//motor pins
#define right_PWM 6
#define left_PWM 7

//servo pins
#define left_servo 18
#define right_servo 20
#define center_servo 21

//stepper
#define right_stepper_DIR 2
#define right_stepper_STEP 3
#define left_stepper_DIR 4  //CW
#define left_stepper_STEP 5 //CLK