#include <Servo.h>

Servo myservoMain;
Servo myservoLeft;
Servo myservoRight;

#define SERVO_MAIN 9
#define SERVO_LEFT 10
#define SERVO_RIGHT 11

int servo_main_up = 100;
int servo_main_down = 5;

void setup(){
  myservoMain.attach(SERVO_MAIN);
  myservoLeft.attach(SERVO_LEFT);
  myservoRight.attach(SERVO_RIGHT);
  
  myservoLeft.write(10);
  myservoRight.write(90);
  myservoMain.write(servo_main_down);

  delay(3000);
  myservoMain.write(servo_main_up);
  delay(3000);
  myservoMain.write(servo_main_down);
  delay(3000);
  myservoLeft.write(25);
  myservoRight.write(25);
  delay(3000);
  myservoLeft.write(0);
  myservoRight.write(90);
} 

void loop(){
  
  delay(3000);
  myservoLeft.write(30);
  myservoRight.write(55);
  delay(3000);
  myservoLeft.write(0);
  myservoRight.write(90);
  
}