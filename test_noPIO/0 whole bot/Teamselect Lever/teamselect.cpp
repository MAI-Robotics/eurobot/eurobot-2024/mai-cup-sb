#define PIN 10
bool state;

void setup() {
    pinMode(PIN, INPUT_PULLUP);
}

void loop() {
    if(digitalRead(PIN) == LOW) {
        digitalWrite(LED_BUILTIN, LOW);
    } else if(digitalRead(PIN) == HIGH) {
        digitalWrite(LED_BUILTIN, HIGH);
    }
}

