#include <Arduino.h>
#include <QTRSensors.h>
#include "pins.h"
#include "sensors.h"
#include "ir-sensor-test.h"

void setup() {
  //QTRX sensor setup
  qtr.setTypeRC();
  qtr.setSensorPins((const uint8_t[]){QTRX_DT1, QTRX_DT2, QTRX_DT3, QTRX_DT4, QTRX_DT5, QTRX_DT6}, irSensorCount);
  qtr.setEmitterPin(QTRX_CTRL);
  
  delay(500);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH); // turn on Arduino's LED to indicate we are in calibration mode

  //QTRX Calibration before serial.begin
  // 2.5 ms RC read timeout (default) * 10 reads per calibrate() call
  // = ~25 ms per calibrate() call.
  // Call calibrate() 400 times to make calibration take about 10 seconds.
  for (uint16_t i = 0; i < 400; i++)
  {
    qtr.calibrate();
  }
  digitalWrite(LED_BUILTIN, LOW); // turn off Arduino's LED to indicate we are through with calibration

  Serial.begin(115200);
  Serial1.begin(115200);

  //prints QTRX Calibration after Serialbeginn
  for (uint8_t i = 0; i < irSensorCount; i++)
  {
    Serial.print(qtr.calibrationOn.minimum[i]);
    Serial.print(' ');
  }
  Serial.println();

  // print the calibration maximum values measured when emitters were on
  for (uint8_t i = 0; i < irSensorCount; i++)
  {
    Serial.print(qtr.calibrationOn.maximum[i]);
    Serial.print(' ');
  }
  Serial.println();
  Serial.println();
  delay(1000);

  //left us sensor setup
  pinMode(US_LEFT_TRIG, OUTPUT);
  pinMode(US_LEFT_ECHO, INPUT);
  pinMode(US_LEFT_FRONT_ECHO, INPUT);
  pinMode(US_LEFT_BACK_ECHO, INPUT);
  //right us sensor setup
  pinMode(US_RIGHT_TRIG, OUTPUT);
  pinMode(US_RIGHT_ECHO, INPUT);
  pinMode(US_RIGHT_FRONT_ECHO, INPUT);
  pinMode(US_RIGHT_BACK_ECHO, INPUT);
  //lower us trig setup  
  pinMode(US_LOWER_TRIG, OUTPUT);
  //front us sensor setup
  pinMode(US_FRONT_LEFT_ECHO, INPUT);
  pinMode(US_FRONT_RIGHT_ECHO, INPUT);
  //back us sensor setup
  pinMode(US_BACK_LEFT_ECHO, INPUT);
  pinMode(US_BACK_RIGHT_ECHO, INPUT);

  pinMode(25,OUTPUT);

  // read us for one time 
  usFilteredValues[0] = readUsSensor(US_LEFT_TRIG, US_LEFT_ECHO);
  usFilteredValues[1] = readUsSensor(US_LEFT_TRIG, US_LEFT_FRONT_ECHO);
  usFilteredValues[2] = readUsSensor(US_LEFT_TRIG, US_LEFT_BACK_ECHO);
  usFilteredValues[3] = readUsSensor(US_RIGHT_TRIG, US_RIGHT_ECHO);
  usFilteredValues[4] = readUsSensor(US_RIGHT_TRIG, US_RIGHT_FRONT_ECHO);
  usFilteredValues[5] = readUsSensor(US_RIGHT_TRIG, US_RIGHT_BACK_ECHO);
  usFilteredValues[6] = readUsSensor(US_LOWER_TRIG, US_FRONT_LEFT_ECHO);
  usFilteredValues[7] = readUsSensor(US_LOWER_TRIG, US_FRONT_RIGHT_ECHO);
  usFilteredValues[8] = readUsSensor(US_LOWER_TRIG, US_BACK_LEFT_ECHO);
  usFilteredValues[9] = readUsSensor(US_LOWER_TRIG, US_BACK_RIGHT_ECHO);

  calibrateQTRX();
  digitalWrite(25,HIGH);

}

void loop() {
  readUS();
  readIR();
  sendData();
  //printData();
}