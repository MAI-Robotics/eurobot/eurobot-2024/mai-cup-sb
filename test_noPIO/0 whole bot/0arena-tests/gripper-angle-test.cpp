#include "globals.h"
#include "pins.h"
#include "communication.h"
#include "logic.h"
#include "servo_drive.h"
#include "stepper_drive.h"
//#include "acceleration.h"

void setup(){
    //limit switch setup
    pinMode(lower_rail_switch, INPUT_PULLUP);
    pinMode(upper_rail_switch, INPUT_PULLUP);
    pinMode(left_limit_switch, INPUT_PULLUP);
    pinMode(right_limit_switch, INPUT_PULLUP);

    //dc motor setup
    pinMode(right_PWM, OUTPUT);
    pinMode(left_PWM, OUTPUT);

    //stepper motor setup
    pinMode(right_stepper_DIR, OUTPUT);
    pinMode(right_stepper_STEP, OUTPUT);
    pinMode(left_stepper_DIR, OUTPUT);
    pinMode(left_stepper_STEP, OUTPUT);

    //servo setup
    LEFT_SERVO.attach(left_servo);
    RIGHT_SERVO.attach(right_servo);
    MAIN_SERVO.attach(center_servo);

    pinMode(25, OUTPUT);
    delay(100);
    liftArm();          //gripper moves up and opens
    openGripperBasket();
    moveUp();

    delay(10000);

    lowerArm();
    closeGripper();
    moveDown();

    delay(2000);

    openGripperBasket();
}
void loop(){
    //nothing in here
}

