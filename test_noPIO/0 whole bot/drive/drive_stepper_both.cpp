/*
  By Emanuel Berger
  02.11.2022
  wiring: https://1drv.ms/u/s!AjWxvMBValH5m2wV5gK-tiDtYfvz?e=FOVCDI
*/
#include "wiring_both.h"
#include "drive.h"


// ------------------- \\


void setup() {
  //Left TB6560
  pinMode(stepPinLeft, OUTPUT); //Step
  pinMode(dirPinLeft, OUTPUT);  //Direction

  //Right TB6600
  pinMode(stepPinRight, OUTPUT); //Step
  pinMode(dirPinRight, OUTPUT);  //Direction
  Serial.begin(9600);
}

void loop() {
  driveCentimeters(FW, 20);
  turnDegrees(R, 720);
  delay(10000);
}
