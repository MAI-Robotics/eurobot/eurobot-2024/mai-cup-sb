bool rotation = false;
bool grip = false;

#include <Servo.h>
Servo LEFT_SERVO;
Servo RIGHT_SERVO;
Servo MAIN_SERVO;

void moveUp(){
    analogWrite(left_PWM, 45);
    analogWrite(right_PWM, 0);
    while((digitalRead(upper_rail_switch)  == HIGH)){
        delay(1);
    }
    analogWrite(left_PWM, 0);
    analogWrite(right_PWM, 0); 
    delay(1000);
}

void moveDown(){
    analogWrite(left_PWM, 0);
    analogWrite(right_PWM, 45);
    while(digitalRead(lower_rail_switch)  == HIGH){
        delay(5);
    }
    analogWrite(left_PWM, 0);
    analogWrite(right_PWM, 0); 
    delay(1000);
}

void liftArm(){
    MAIN_SERVO.write(180);
}

void lowerArm(){
  MAIN_SERVO.write(10);
}

void openGripperBasket(){
    LEFT_SERVO.write(70);
    RIGHT_SERVO.write(130);
 
}

void closeGripper(){
    LEFT_SERVO.write(130);
    RIGHT_SERVO.write(70);
}