#include <Arduino.h>
#include <AccelStepper.h>
#include <MultiStepper.h>
#include "pins.h"


//AccelStepper Xaxis(1, STEP_PIN, DIR_PIN);
AccelStepper mystepper(1, right_stepper_STEP, right_stepper_DIR);
AccelStepper mystepper1(1, left_stepper_STEP, left_stepper_DIR);

MultiStepper steppers;

void setup() {
  mystepper.setMaxSpeed(900);
  mystepper.setAcceleration(100);
  mystepper1.setMaxSpeed(900);
  mystepper1.setAcceleration(100);

  steppers.addStepper(mystepper);
  steppers.addStepper(mystepper1);

}

void loop() {
  long positions[2]; // Array of desired stepper positions
  
  positions[0] = -1000;
  positions[1] = -1000;
  steppers.moveTo(positions);
  mystepper.setAcceleration(100);
  mystepper1.setAcceleration(100);
  steppers.runSpeedToPosition(); // Blocks until all are in position
  delay(1000);
  
  // Move to a different coordinate
  positions[0] = 1000;
  positions[1] = 1000;
  steppers.moveTo(positions);
  mystepper.setAcceleration(100);
  mystepper1.setAcceleration(100);
  steppers.runSpeedToPosition(); // Blocks until all are in position
  delay(1000);
}