//arduino mega

void setup() {
  Serial.begin(9600);
}

void loop() {
  if (Serial.available() > 0) {
    int incomingValue = Serial.parseInt();
    Serial.println("Received value: " + String(incomingValue));
    if (incomingValue == 100) {
      Serial.println("Test successful");
    }
  } else {
    Serial.println("No data received");
  }
}
