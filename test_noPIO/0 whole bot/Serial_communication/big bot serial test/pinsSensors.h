//ultrasonic sensors

    #define US_FRONT_RIGHT_TRIG_PIN 4   
    #define US_FRONT_RIGHT_ECHO_PIN 5

    #define US_FRONT_LEFT_TRIG_PIN 27  
    #define US_FRONT_LEFT_ECHO_PIN 26

    #define US_RIGHT_TRIG_PIN 6  
    #define US_RIGHT_ECHO_PIN 7

    #define US_BACK_RIGHT_TRIG_PIN 22  
    #define US_BACK_RIGHT_ECHO_PIN 28

    #define US_BACK_LEFT_TRIG_PIN 17
    #define US_BACK_LEFT_ECHO_PIN 16

    #define US_LEFT_TRIG_PIN 19
    #define US_LEFT_ECHO_PIN 18

//infrared sensor array
#define IR_1_PIN 19
#define IR_2_PIN 20
#define IR_3_PIN 21
#define IR_4_PIN 22
#define IR_5_PIN 24 
#define IR_6_PIN 25
#define IR_CTRL_PIN 26