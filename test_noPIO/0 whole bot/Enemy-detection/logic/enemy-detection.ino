#include "sensors.h"
#include "wiring.h"
#include "drive.h"

void setup() {
    Serial.begin(9600);                 // Die serielle Kommunikation starten
    pinMode(TRIGGER_S1, OUTPUT);           // Trigger Pin als Ausgang definieren
    pinMode(ECHO_S1, INPUT);               // Echo Pin als Eingang defnieren
}

void loop() {
    while(readCentimeters(1) <= 10 || readCentimeters(2) <= 10) {
        Serial.println("Gegner erkannt.");
        //stehen bleiben code hier
    }
}
