//AI-Generated Code 
//@ 17.1.23 

// Hier sind die Pins für die Ultraschallsensoren definiert 
#define ULTRA_SENSOR_LEFT_PIN 3 
#define ULTRA_SENSOR_RIGHT_PIN 4

//Eine Variable speichert die gemessene Entfernung
long gegnerEntfernung = 0;

void setup() {
  //Festlegen der Pinmodes
  pinMode(ULTRA_SENSOR_LEFT_PIN, INPUT);
  pinMode(ULTRA_SENSOR_RIGHT_PIN, INPUT); 
}

void loop() {
  //Das Skript misst die Entfernung zu einem Gegnerroboter mit den beiden Ultraschallsensoren.
  long leftDist = getPing(ULTRA_SENSOR_LEFT_PIN, 10);
  long rightDist = getPing(ULTRA_SENSOR_RIGHT_PIN, 10);
 
  //Die Entfernung wird auf einen bestimmten Bereich limitiert, 
  //besonders wenn sie kleiner als das Minimum (hier 1 Zentimeter) ist
  if (leftDist < 1) leftDist = 1;
  if (rightDist < 1) rightDist = 1;

  //Bestimmung der Entfernung, indem der Mittelwert der beiden Sichtrichtungen gebildet wird
  gegnerEntfernung = (leftDist + rightDist) / 2;
  
  //Hier kann das Skript weitere Aktionen ausführen, 
  //wenn ein Gegnerroboter in einem bestimmten Entfernungsbereich ist.
   if (gegnerEntfernung < 10) {
	//Führe irgendeine Aktion aus.
   }
}

//Die getPing() Funktion ist eine Funktion, die sich darauf spezialisiert hat, die Entfernung mit einem Ultraschallsensor zu messen. 
long getPing(byte triggerPin, byte echoPin, int timeout) {
   long  duration, distance;
 
   // Send the trigger signal
   digitalWrite(triggerPin, HIGH);
   delayMicroseconds(10);
   digitalWrite(triggerPin, LOW);
 
   // Now wait for the echo which represents the time required for the 
   // signal to travel to the obstacle and return.
   duration = pulseIn (echoPin, HIGH, timeout);
 
   if (duration == 0) {
     distance = 0;
   } else {
     // The speed of sound is 340 m/s or 29 microseconds per centimeter.
     // The ping travels out and back, so to find the distance of the
     // object we take half of the distance travelled.
     distance = duration/29/2;
   }
 
   return distance;
}
