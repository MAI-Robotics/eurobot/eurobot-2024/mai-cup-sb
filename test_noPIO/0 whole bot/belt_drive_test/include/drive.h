void moveUp(){
    analogWrite(left_PWM, 45);
    analogWrite(right_PWM, 0);
    digitalWrite(25,LOW);
    while((digitalRead(upper_rail_switch)  == HIGH)){
        delay(1);
    }
    digitalWrite(25,HIGH);
    analogWrite(left_PWM, 0);
    analogWrite(right_PWM, 0); 
    delay(1000);
}

void moveDown(){
    analogWrite(left_PWM, 0);
    analogWrite(right_PWM, 45);
    while(digitalRead(lower_rail_switch)  == HIGH){
        delay(5);
    }
    analogWrite(left_PWM, 0);
    analogWrite(right_PWM, 0); 
    delay(1000);
}