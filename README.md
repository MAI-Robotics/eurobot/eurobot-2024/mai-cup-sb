# MAI Cup 2022/23
Repo of the secondary bot project of Eurobot 2022/23 participating in the MAI Cup.
This project is part of [mai-robotics.de](https://mai-robotics.de).

Wiring for testing and final version: [Link](https://1drv.ms/u/s!AjWxvMBValH5lCZe_igv6-B3hBiS?e=f8aBv1)
Wiring diagram files: [Link](https://1drv.ms/u/s!AjWxvMBValH5gpd5g7pLX6WjrYQN9w?e=v0Fwiv)
Fritzing parts we used/made ourselves are located [here](https://1drv.ms/u/s!AjWxvMBValH5lDhznWpF68caogW8?e=kVtLpQ).

## Images
Real Bot
![Picture_1](images/week%2022/IMG_3018.jpeg)

## 3D Models
- Main 3D Model ([Link](https://a360.co/3JjTvWj))

## Structure of Repo
We have a folder for test files and a src folder, containing both picos.

## Coordinate system 
See small-bot/docs/ for measuring-tools