#include "globals.h"


void driveFast(unsigned int steps, bool dir){         
    digitalWrite(left_stepper_DIR, dir ? 0 : 1);        
    digitalWrite(right_stepper_DIR, dir ? 0 : 1);

    delay(10);

    float a = 0; // for acceleration
    for (unsigned int i = 0; i < steps; i++)
    {
        a = 0;
        if (i < 400 && i <= steps / 2)           //acceleration section
        {
            a = tPerStep * 0.002 * (400 - i);
        }

        if (steps - i < 400 && i > steps / 2)    //deceleration section
        {
            a = tPerStep * 0.002 * (400 - (steps - i));
        }


        digitalWrite(left_stepper_STEP, HIGH);
        digitalWrite(right_stepper_STEP, HIGH);
        delayMicroseconds(tPerStep + a);
        digitalWrite(left_stepper_STEP, LOW);
        digitalWrite(right_stepper_STEP, LOW);
        delayMicroseconds(tPerStep + a);
    }
    delay(10);
}


/*
void turnFast(unsigned int steps, bool dir){        
    digitalWrite(DIR_PIN_LEFT, dir ? 1 : 0);        
    digitalWrite(DIR_PIN_RIGHT, dir ? 1 : 0);
    digitalWrite(ENABLE_PIN, LOW);

    delay(10);

    float a = 0; // for acceleration
    for (unsigned int i = 0; i < steps; i++)
    {
        a = 0;
        if (i < 400 && i <= steps / 2)             //acceleration section
        {
            a = tPerStep * 0.003 * (400 - i);
        }

        if (steps - i < 400 && i > steps / 2)       //deceleration section
        {
            a = tPerStep * 0.003 * (400 - (steps - i));
        }

        digitalWrite(STEP_PIN, HIGH);
        delayMicroseconds(tPerStep + a);
        digitalWrite(STEP_PIN, LOW);
        delayMicroseconds(tPerStep + a);
    }

}
*/