#include "servo_drive.h"
#include "logic.h"
#include "pins.h"

Servo LEFT_SERVO;
Servo RIGHT_SERVO;
Servo MAIN_SERVO;

void moveUp(){
    analogWrite(left_PWM, beltDriveSpeed);
    analogWrite(right_PWM, 0);
    while((digitalRead(upper_rail_switch)  == HIGH)){
        delay(5);
        stopTimeout();
    }
    analogWrite(left_PWM, 0);
    analogWrite(right_PWM, 0); 
    delay(1000);
}

void moveDown(){
    analogWrite(left_PWM, 0);
    analogWrite(right_PWM, beltDriveSpeed);
    while(digitalRead(lower_rail_switch)  == HIGH){
        delay(5);
        stopTimeout();
    }
    analogWrite(left_PWM, 0);
    analogWrite(right_PWM, 0); 
    delay(1000);
}

void liftArm(){
    stopTimeout();
    MAIN_SERVO.write(MAIN_SERVO_LIFT);
}

void lowerArm(){
    stopTimeout();
    MAIN_SERVO.write(MAIN_SERVO_LOWER);
}

void openGripperBasket(){
    stopTimeout();
    LEFT_SERVO.write(LEFT_SERVO_OPENBASKET);
    RIGHT_SERVO.write(RIGHT_SERVO_OPENBASKET);
}

void openGripper(){
    stopTimeout();
    LEFT_SERVO.write(LEFT_SERVO_OPEN);
    RIGHT_SERVO.write(RIGHT_SERVO_OPEN);
}

void closeGripper(){
    stopTimeout();
    LEFT_SERVO.write(LEFT_SERVO_CLOSE);
    RIGHT_SERVO.write(RIGHT_SERVO_CLOSE);
}

void detachServos() {
    LEFT_SERVO.detach();
    RIGHT_SERVO.detach();
    MAIN_SERVO.detach();
}