#include "main.h"


void setup(){
    Serial.begin(9600);
    Serial1.begin(115200);

    DEBUG && Serial.println("START BOT " + softwareVersion);

    startUpRoutine(false);     // pins, gripper, waiting for pullcord (working)
}

void loop(){
  /**
  * Working tactics:
  * defaultTactics();
  * homeZoneTactics();
  * preloadingTactics();
  * 
  * Not finally tested:
  * riskyTactics();
  */

  defaultTactics();

  detachServos();

  while(true) {
    delay(500);
  }
}

void setup1(){
  // code goes here
}

void loop1(){ // only for receiving data
  Serial1.readStringUntil('\n').toCharArray(inputString, STRING_SIZE); //for deactivating the enemy detection
  
  // Serial.println(inputString);
  getSensorValues();  // us and ir values
  DEBUG && Serial.print("X: " + String(x_pos) + " Y: " + String(y_pos) + " DIR: " + String(direction) + "\n");
}


/**
*   Tactics 
*/

void defaultTactics() {   // normal

  setPos(2000, 300, 90);

  waitForPullcord();
  
  driveDistanceMM(2500, 0, true, No, No);

  while(true) {
    turnAngle(180, true);
  }

  // driveTo(750, 1550); //first ball
  // driveTo(650, maxYHeight);

  // driveTo(2000, 850); //second ball
  // driveTo(2000, maxYHeight);

  // driveTo(3300, 850); //third ball
  // driveTo(3300, maxYHeight);

  // driveTo(2650, 850); //random ball locations #1
  // driveTo(2650, maxYHeight);

  // driveTo(1300, 850); //random ball locations #2
  // driveTo(1300, maxYHeight);

  // driveTo(2000, 300); //end zone
  // turnTo(90);

  // endingRoutine();
}
