#include "communication.h"
#include "stepper_drive.h"
#include "stepper_drive.h"
#include "logic.h"

char inputString[STRING_SIZE]; //Initialized variable to store recieved data
long int outputArray[arraySize];

int usValues[US_SENSORS];
int irValues[IR_SENSORS];

uint16_t LINE_POSITION = 0;

int distanceDriven = 0;
bool left_detected = false;
bool center_detected = false;
bool right_detected = false;


void splitString() { 
    int i = 0;
    char *p = strtok(inputString, ";");

    while(p != NULL) {
        //Serial.print("Section found: ");
        //Serial.println(p);

        outputArray[i++] = atof(p);
        p = strtok(NULL, ";");
    }
}

void getSensorValues() {    
    splitString();
    
    DEBUG && Serial.print("US Sensor Values: ");
    for(int i = 0; i < US_SENSORS; i++){
        usValues[i] = outputArray[i];
        DEBUG && Serial.print(usValues[i]);
        DEBUG && Serial.print(", ");
    }
    DEBUG && Serial.println("");

    DEBUG && Serial.print("IR Sensor Values: ");         //working @4.4.23
    for(int i = 0; i < IR_SENSORS; i++){
        irValues[i] = outputArray[i+US_SENSORS];
        DEBUG && Serial.print(irValues[i]);
        DEBUG && Serial.print(", "); 
    }

    if(((irValues[0] < 900) || (irValues[1] < 900) ||(irValues[2] < 900) || (irValues[3] < 900) || (irValues[4] < 900) || (irValues[5] < 900)) && ((irValues[0] > 0) && (irValues[1] > 0) && (irValues[2] > 0) && (irValues[3] > 0) && (irValues[4] > 0) && (irValues[5] > 0))) {
        DEBUG && Serial.print("LINIE ERKANNT");
    } else {
        DEBUG && Serial.print("LINIE NICHT ERKANNT");
    }

    DEBUG && Serial.println("");
}
