#ifndef MAIN_H
#define MAIN_H
//begin of .h file

#include "globals.h"
#include "pins.h"
#include "communication.h"
#include "logic.h"
#include "servo_drive.h"
#include "stepper_drive.h"
//#include "acceleration.h"

void defaultTactics();
void riskyTactics();
void riskyTactics2();
void homeZoneTactics();
void preloadingTactics(bool DITEnabled = false);
void homologation();


#endif