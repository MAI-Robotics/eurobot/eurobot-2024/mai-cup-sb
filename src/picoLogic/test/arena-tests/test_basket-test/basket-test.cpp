#include "globals.h"
#include "pins.h"
#include "communication.h"
#include "logic.h"
#include "servo_drive.h"
#include "stepper_drive.h"
//#include "acceleration.h"

void setup(){
    //limit switch setup
    pinMode(lower_rail_switch, INPUT_PULLUP);
    pinMode(upper_rail_switch, INPUT_PULLUP);
    pinMode(left_limit_switch, INPUT_PULLUP);
    pinMode(right_limit_switch, INPUT_PULLUP);

    //dc motor setup
    pinMode(right_PWM, OUTPUT);
    pinMode(left_PWM, OUTPUT);

    //stepper motor setup
    pinMode(right_stepper_DIR, OUTPUT);
    pinMode(right_stepper_STEP, OUTPUT);
    pinMode(left_stepper_DIR, OUTPUT);
    pinMode(left_stepper_STEP, OUTPUT);

    //servo setup
    LEFT_SERVO.attach(left_servo);
    RIGHT_SERVO.attach(right_servo);
    MAIN_SERVO.attach(center_servo);

    pinMode(25, OUTPUT);
    delay(100);
    //driveFast(2000, true, false);
    liftArm();  
    delay(1000);
    lowerArm();
    //start
    openGripperBasket();
    lowerArm();
    moveUp();
    delay(1000);

    driveMaxUntilSwitch(true, false);
    //driveUntilSwitch(10000, true, false);   //drive to cherry holder
    delay(1000);
    moveDown();
    closeGripper();
    delay(1000);

    moveUp();
    liftArm();
    driveDistanceMM(100, 1, false, No, false);
    delay(1000);

    driveMaxUntilSwitch(true, false);  //drive to cherry basket
    delay(1000);
    openGripper();
    delay(2000);

    closeGripper();
    driveDistanceMM(100, 239784, false, No, false);
    delay(1000);
    lowerArm();
}
void loop(){
    //nothing in here
}

