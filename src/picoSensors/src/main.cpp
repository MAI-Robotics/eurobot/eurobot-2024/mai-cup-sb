#include "globals.h"
#include "pins.h"
#include "sensors.h"
#include "logic.h"


void setup() {
  Serial.begin(9600);   //PC
  Serial1.begin(115200);

  setPins();
  readUS(false);
  // calibrateQTRX();

  digitalWrite(25,HIGH);
}

void loop() {
  readIR();     //working
  sendData();   //working
  
  if(DEBUG) printData();  //working
}

void loop1() {
  readUS(false);     //on second core because of speed issues, working
        //false while homologation, true while driving (more errors with false but faster reaction times)
}