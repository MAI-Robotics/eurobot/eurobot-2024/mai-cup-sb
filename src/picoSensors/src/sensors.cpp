#include "sensors.h"
#include "pins.h"
#include "logic.h"

//QTRX sensor
// sensors 0 through 7 are connected to digital pins 3 through 10, respectively
QTRSensorsRC qtr((unsigned char[]) {QTRX_DT1, QTRX_DT2, QTRX_DT3, QTRX_DT4, QTRX_DT5, QTRX_DT6}, NUM_IR, TIMEOUT, QTRX_CTRL);
float irFilteredValues[NUM_IR+1];
unsigned int irRawValues[NUM_IR];
unsigned int irRawPosition;

//ultrasonic sensors
float usFilteredValues[9];



int filter(int value, float* lastValue){
    float a = 0.3;

    //Serial.print(value);
    //Serial.print(",");

    float filteredValue = ((1-a)*(*lastValue)) + (a*value);
    *lastValue = filteredValue;

    //Serial.println(filteredValue);

    return filteredValue;

}

int readUsSensor(int trigPin, int echoPin){
    // Clears the TRIG_PIN
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    // Sets the TRIG_PIN on HIGH state for 10 micro seconds
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    // Reads the ECHO_PIN, returns the sound wave travel time in microseconds
    int duration = pulseIn(echoPin, HIGH);
    // Calculating the distance
    int distance = duration * 0.034 / 2;

    return distance;
}

void readUS(bool filterValues){
    if(filterValues) {
        // read and filter us sensors
        usFilteredValues[0] = filter(readUsSensor(US_LEFT_TRIG, US_LEFT_ECHO), &usFilteredValues[0]);
        if(readUsDelay > 0) delay(readUsDelay);
        // usFilteredValues[1] = filter(readUsSensor(US_LEFT_TRIG, US_LEFT_FRONT_ECHO), &usFilteredValues[1]);
        // if(readUsDelay > 0) delay(readUsDelay);
        // usFilteredValues[2] = filter(readUsSensor(US_LEFT_TRIG, US_LEFT_BACK_ECHO), &usFilteredValues[2]);
        // if(readUsDelay > 0) delay(readUsDelay);
        usFilteredValues[3] = filter(readUsSensor(US_RIGHT_TRIG, US_RIGHT_ECHO), &usFilteredValues[3]);
        if(readUsDelay > 0) delay(readUsDelay);
        // usFilteredValues[4] = filter(readUsSensor(US_RIGHT_TRIG, US_RIGHT_FRONT_ECHO), &usFilteredValues[4]);
        // if(readUsDelay > 0) delay(readUsDelay);
        // usFilteredValues[5] = filter(readUsSensor(US_RIGHT_TRIG, US_RIGHT_BACK_ECHO), &usFilteredValues[5]);
        // if(readUsDelay > 0) delay(readUsDelay);
        usFilteredValues[6] = filter(readUsSensor(US_LOWER_TRIG, US_FRONT_LEFT_ECHO), &usFilteredValues[6]);
        if(readUsDelay > 0) delay(100);
        // usFilteredValues[7] = filter(readUsSensor(US_LOWER_TRIG, US_FRONT_RIGHT_ECHO), &usFilteredValues[7]);
        // if(readUsDelay > 0) delay(100);
        // usFilteredValues[8] = filter(readUsSensor(US_LOWER_TRIG, US_BACK_LEFT_ECHO), &usFilteredValues[8]);
        // if(readUsDelay > 0) delay(readUsDelay);
        usFilteredValues[9] = filter(readUsSensor(US_LOWER_TRIG, US_BACK_RIGHT_ECHO), &usFilteredValues[9]);
        if(readUsDelay > 0) delay(readUsDelay);
    } else {
        usFilteredValues[0] = readUsSensor(US_LEFT_TRIG, US_LEFT_ECHO);
        if(readUsDelay > 0) delay(readUsDelay);
        // usFilteredValues[1] = readUsSensor(US_LEFT_TRIG, US_LEFT_FRONT_ECHO);
        // if(readUsDelay > 0) delay(readUsDelay);
        // usFilteredValues[2] = readUsSensor(US_LEFT_TRIG, US_LEFT_BACK_ECHO);
        // if(readUsDelay > 0) delay(readUsDelay);
        usFilteredValues[3] = readUsSensor(US_RIGHT_TRIG, US_RIGHT_ECHO);
        if(readUsDelay > 0) delay(readUsDelay);
        // usFilteredValues[4] = readUsSensor(US_RIGHT_TRIG, US_RIGHT_FRONT_ECHO);
        // if(readUsDelay > 0) delay(readUsDelay);
        // usFilteredValues[5] = readUsSensor(US_RIGHT_TRIG, US_RIGHT_BACK_ECHO);
        // if(readUsDelay > 0) delay(readUsDelay);
        usFilteredValues[6] = readUsSensor(US_LOWER_TRIG, US_FRONT_LEFT_ECHO);
        if(readUsDelay > 0) delay(100);
        // usFilteredValues[7] = readUsSensor(US_LOWER_TRIG, US_FRONT_RIGHT_ECHO);
        // if(readUsDelay > 0) delay(100);
        // usFilteredValues[8] = readUsSensor(US_LOWER_TRIG, US_BACK_LEFT_ECHO);
        // if(readUsDelay > 0) delay(readUsDelay);
        usFilteredValues[9] = readUsSensor(US_LOWER_TRIG, US_BACK_RIGHT_ECHO);
        if(readUsDelay > 0) delay(readUsDelay);
    }
}

void readIR(){
    
    // read raw ir values 
    irRawPosition = qtr.readLine(irRawValues);

    // filter raw ir values
    for (int i = 0; i < NUM_IR; i++)
    {  
        irFilteredValues[i] = filter(irRawValues[i], &irFilteredValues[i]);
    }
    irFilteredValues[NUM_IR] = filter(irRawPosition, &irFilteredValues[NUM_IR]);

}

void sendData(){
    for(int i = 0; i < NUM_US; i++){
        Serial1.print(usFilteredValues[i]);
        Serial1.print(";");
    }

    for (int j = 0; j < NUM_IR; j++){
        Serial1.print(irFilteredValues[j]);
        Serial1.print(";");
    }

    Serial1.println(irRawPosition);
    delay(delayPerValue);                   //limit amount of values to valuesPerSec
}

void printData(){
    
    Serial.println("US");

    for(int i = 0; i < NUM_US; i++){
        Serial.print(i+1);
        Serial.print(": ");
        Serial.println(usFilteredValues[i]);

    }

    Serial.println("IR");

    for (int j = 0; j < NUM_IR; j++){
        Serial.print(j+1);
        Serial.print(": ");
        Serial.println(irFilteredValues[j]);
    }

    Serial.print("Position: ");

    Serial.println(String(qtr.readLine(irRawValues)));

}


void calibrateQTRX(){
    for (uint16_t i = 0; i < 400; i++)
    {
        qtr.calibrate();
    }

    // print the calibration minimum values measured when emitters were on
    for (uint8_t i = 0; i < NUM_IR; i++)
    {
        DEBUG && Serial.print(qtr.calibratedMinimumOn[i]);
        DEBUG && Serial.print(' ');
    }
    DEBUG && Serial.println();

    // print the calibration maximum values measured when emitters were on
    for (uint8_t i = 0; i < NUM_IR; i++)
    {
        DEBUG && Serial.print(qtr.calibratedMaximumOn[i]);
        DEBUG && Serial.print(' ');
    }
    DEBUG && Serial.println();
    delay(1000);
}

