#ifndef LOGIC_H
#define LOGIC_H
//begin of .h file

#include "globals.h"

const bool DEBUG = false;       // enable debug messages in serial monitor

const int readUsDelay = 1;      // delay between us-sensor-query

void setPins();

#endif